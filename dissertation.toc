\contentsline {chapter}{\numberline {0}Introduction}{1}
\contentsline {chapter}{\numberline {1}Polytopes and optimisation}{3}
\contentsline {section}{\numberline {1.1}Polytopes and Polyhedra}{3}
\contentsline {subsection}{\numberline {1.1.1}The dual polytope}{6}
\contentsline {subsection}{\numberline {1.1.2}Weyl's and Minkowski's theorems for polytopes}{9}
\contentsline {section}{\numberline {1.2}Linear programming and linear programming duality}{10}
\contentsline {chapter}{\numberline {2}Algorithms for linear programming}{13}
\contentsline {section}{\numberline {2.1}Overview}{13}
\contentsline {subsection}{\numberline {2.1.1}The Simplex Method}{14}
\contentsline {subsection}{\numberline {2.1.2}The Ellipsoid Method}{15}
\contentsline {subsection}{\numberline {2.1.3}Interior point method(s)}{16}
\contentsline {section}{\numberline {2.2}The Yamnitsky-Levin algorithm}{17}
\contentsline {section}{\numberline {2.3}The Multiplicative Weights Update Method}{21}
\contentsline {subsection}{\numberline {2.3.1}Plotkin, Shmoys, Tardos framework for LPs}{24}
\contentsline {subsection}{\numberline {2.3.2}An oracle for fractional packing LPs}{26}
\contentsline {chapter}{\numberline {3}Packing algorithms and the YL algorithm}{28}
\contentsline {section}{\numberline {3.1}The min-volume packing problem}{28}
\contentsline {subsection}{\numberline {3.1.1}The min-volume problem as a packing LP}{33}
\contentsline {subsection}{\numberline {3.1.2}The Yamnitsky-Levin algorithm, revisited}{38}
\contentsline {section}{\numberline {3.2}Better algorithms: Learning oracle and adaptive step size}{40}
\contentsline {subsection}{\numberline {3.2.1}The learning oracles}{40}
\contentsline {subsection}{\numberline {3.2.2}A simpler algorithm}{45}
\contentsline {section}{\numberline {3.3}Final considerations for a complete algorithm.}{48}
\contentsline {chapter}{\numberline {4}Conclusions}{50}
\contentsline {chapter}{Bibliography}{50}
