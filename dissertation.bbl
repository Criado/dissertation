\begin{thebibliography}{1}

\bibitem{MWpaper}
Sanjeev Arora, Elad Hazan, and Satyen Kale.
\newblock The multiplicative weights update method: a meta-algorithm and
  applications.
\newblock {\em Theory of Computing}, 8(1):121--164, 2012.

\bibitem{YL2paper}
Sven~G Bartels.
\newblock The complexity of yamnitsky and levin’s simplices algorithm.
\newblock In {\em Polytopes—Combinatorics and Computation}, pages 199--225.
  Springer, 2000.

\bibitem{PTASpaper}
Christos Koufogiannakis and Neal~E Young.
\newblock A nearly linear-time ptas for explicit fractional packing and
  covering linear programs.
\newblock {\em Algorithmica}, 70(4):648--674, 2014.

\bibitem{leebook}
Jon Lee.
\newblock {\em A first course in combinatorial optimization}, volume~36.
\newblock Cambridge University Press, 2004.

\bibitem{PSTpaper}
Serge~A Plotkin, David~B Shmoys, and {\'E}va Tardos.
\newblock Fast approximation algorithms for fractional packing and covering
  problems.
\newblock {\em Mathematics of Operations Research}, 20(2):257--301, 1995.

\bibitem{santospaper}
Francisco Santos.
\newblock A counterexample to the hirsch conjecture.
\newblock {\em arXiv preprint arXiv:1006.2814}, 2010.

\bibitem{YLpaper}
Boris Yamnitsky and Leonid~A Levin.
\newblock An old linear programming algorithm runs in polynomial time.
\newblock In {\em Foundations of Computer Science, 1982. SFCS'08. 23rd Annual
  Symposium on}, pages 327--328. IEEE, 1982.

\end{thebibliography}
